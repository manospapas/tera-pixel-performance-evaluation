# Add any project specific configuration here.
add.config(
  apply.override = FALSE
)

# Add project specific configuration that can be overridden from load.project()
add.config(
  apply.override = TRUE
)

# Relative path for storing raw data.
add.config(raw_data_path = "./data/raw_data/")

# Most used file pattern. In our cases is csv.
add.config(file_pattern = "*.csv")

# List of files.
add.config(file_names = list.files(pattern = config$file_pattern, path = config$raw_data_path))
